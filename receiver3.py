# Basic multicast receiver setup that will get data from sender

import socket
import signal
import sys
import os
import subprocess
import shlex
import time
from struct import pack
from unireedsolomon import rs

def sig_int_handler(signal, frame):

	print "RECEIVER3 CLOSING"
	sys.exit(0)

def get_binary(s):

	bin_s = ""
	for char in s:
		bin_s += format(ord(char), '08b')
	return bin_s
	
def find_parity(s):

	num = 0
	for char in s:
		if char == "1":
			num += 1
	return num % 2

def create_jpeg(data, f_num, dir_name):

	with open(dir_name + "/frame" + str(f_num) + ".jpg", "wb") as img:
		img.write(data)
	print "WROTE FRAME"

def convert_to_byte_array(p_data):

	b = bytearray()
	i = 0
	while i * 8 < len(p_data):
		byte = int(p_data[i * 8: i * 8 + 8], 2)
		b.append(byte)
		i += 1
	return b

def fill_frame_data(index, b_array, frame_data, last_packet_num):

	start = 220 * index
	for byte in b_array:
		frame_data[start] = byte 
		start += 1
	if index == last_packet_num:
		return frame_data[:start]
	else:
		return frame_data

def main():


	# handle server interrupts such as Ctrl-C
	signal.signal(signal.SIGINT, sig_int_handler)

	# create socket object that supports IP4 address family and uses UDP
	receiver = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	receiver.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

	# bind server to host_name and port 
	host = ""
	port = 7777
	receiver.bind((host, port))

	print "RECEIVER3 STARTING"

	# add server to multicast group and set multicast options
	multicast_group = "225.5.55.55"
	group = socket.inet_aton(multicast_group)
	mreq = pack('4sL', group, socket.INADDR_ANY)
	receiver.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

	dir_name = "rc3frames"
	pwd = os.getcwd()
	if dir_name in os.listdir(pwd):
		try:
			args = shlex.split("rm -r " + dir_name)
			subprocess.check_call(args)
		except subprocess.CalledProcessError:
			print "Error with removing receiver3's frames directory"
			sys.exit(1)

	# create directory for received jpg frames
	os.mkdir(dir_name)

	# find right buffer size for receiving based on packet size
	packet_size = 255
	power = 0
	while 2 ** power < packet_size:
		power += 1
	buf_size = 2 ** power

	try:

		frame_data = bytearray()
		frame_num = 0
		total_packets = 0
		num_packets, num_good, num_lost, num_corrupt = 0, 0, 0, 0
		seenFirst = False
		prev_highest= -1
		processed = {}
		flag_seq_num = 0
		decode_flag = True
		write_flag = False
		coder = rs.RSCoder(255, 223)

		while True:
			
			# receive sender's connection message and address
			sender_data, sender_addr = receiver.recvfrom(buf_size)

			# receive last frame packet signal, ask for retransmissions for empty slots in frame array 
			if sender_data == "EOF":

				write_flag = True
				all_filled = True
				for packet_num in range(0, total_packets):
					if packet_num not in processed:
						req = "RP" + str(packet_num) 
						receiver.sendto(req, sender_addr)
						all_filled = False 
				if all_filled:
					receiver.sendto("R3 DONE ACK", sender_addr)

			# receive info for total number of packets for upcoming frame
			elif "META" in sender_data and not seenFirst:
				start = time.time()
				
				total_packets = int(sender_data[4:])
				print "Number of packets for the upcoming frame is %d" %(total_packets)
				flag_seq_num = total_packets - 2
				frame_data = bytearray(220 * total_packets)
				receiver.sendto("R3 START ACK", sender_addr)
				seenFirst = True
				decode_flag = True
				prev_highest= -1

			elif "META" in sender_data and seenFirst:

				print processed
				processed = {}
				if write_flag:
					create_jpeg(frame_data, frame_num, dir_name)
					print time.time() - start
					write_flag = False
					frame_num += 1
					prev_highest= -1

				total_packets = int(sender_data[4:])
				print "Number of packets for the upcoming frame is %d" %(total_packets)
				flag_seq_num = total_packets - 2
				decode_flag = True
				frame_data = bytearray(220 * total_packets)
				receiver.sendto("R3 START ACK", sender_addr)

			else:

				if len(sender_data) < 255:
					decode_flag = False 

				if decode_flag:

					try:
						decoded_data = coder.decode_fast(sender_data)[0]
						if len(decoded_data) < 223:
							decoded_data = chr(0) + decoded_data
					except rs.RSCodecError:
						decoded_data = sender_data

				else:
					decoded_data = sender_data
					decode_flag = True

				packet_data_string = decoded_data[:-3]
				frame_byte_string = decoded_data[-3]
				middle_byte_string = decoded_data[-2]
				last_byte_string = decoded_data[-1]

				packet_data_bin = get_binary(packet_data_string)
				frame_byte_bin = get_binary(frame_byte_string)
				middle_byte_bin = get_binary(middle_byte_string)
				last_byte_bin = get_binary(last_byte_string)

				num_packets += 1

				# parse sequence num
				seq_bin = middle_byte_bin[5:] + last_byte_bin[:7]
				sequence_num = int(seq_bin, 2)
				print sequence_num

				if sequence_num > total_packets:
					continue

				# check for packet corruption
				if int(last_byte_bin[7]) != find_parity(packet_data_bin):
					print "ERROR WITH PARITY"
					req = "RP" + str(sequence_num) 
					print req
					receiver.sendto(req, sender_addr)
					continue

				if sequence_num == flag_seq_num:
					decode_flag = False

				# check for packet loss by checking whether current packet is in sequence with previous
				if sequence_num not in processed:
					processed[sequence_num] = True
					if sequence_num > prev_highest + 1:
						# request retransmission from [prev_highest+ 1, sequence_num - 1]
						for i in range(prev_highest + 1, sequence_num):
							req = "RP" + str(i) 
							print req
							receiver.sendto(req, sender_addr)
						prev_highest = sequence_num
					else:
						if sequence_num > prev_highest:
							prev_highest = sequence_num

					frame_data = fill_frame_data(sequence_num, convert_to_byte_array(packet_data_bin), frame_data, total_packets - 1)

	finally:

		receiver.close()

if __name__ == "__main__":
	main()
