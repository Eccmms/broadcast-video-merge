# Outputs all frames of video in new directory specified 
# USAGE: Pass in video_file name from commandline or call main
# from another file

import cv2
import sys
import os

def main(name):

	video_name = name
	vidcap = cv2.VideoCapture(video_name)
	readProperly = True
	frame_count = 0

	cur_dir = os.getcwd()
	new_dir = "frames"
	os.mkdir(new_dir)
	os.chdir(new_dir)

	while readProperly:
		readProperly, image = vidcap.read()
		if readProperly:
			cv2.imwrite("frame%d.jpg" %frame_count, image)
			frame_count += 1

	os.chdir(cur_dir)

	return frame_count
	
if __name__ == "__main__":

	if len(sys.argv) != 2:
		print "Usage: python %s video_file_name" %(sys.argv[0])
	else:
		main(sys.argv[1])
