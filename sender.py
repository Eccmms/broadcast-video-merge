# Basic multicast sender setup that will send receivers in group data

import socket
import signal
import sys
import setup
from struct import pack
from math import ceil
from unireedsolomon import rs
from queue import Queue

def sig_int_handler(signal, frame):

	print "SERVER CLOSING"
	sys.exit(0)

def main():

	# handle server interrupts such as Ctrl-C
	signal.signal(signal.SIGINT, sig_int_handler)

	multicast_group = ("225.5.55.55", 7777)

	# Create IPv4 UDP socket
	sender = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

	# Time out while receiving data.
	sender.settimeout(0.2)

	# set TTL to control how many networks will receive packet
	ttl = pack('b', 1)
	sender.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl)

	packet_size = 220
	frames_payloads = setup.init()
	num_receivers = 2

	for frame_num in range(0, len(frames_payloads)):

		coder = rs.RSCoder(255, 223)
		q = Queue()
		payload = frames_payloads[frame_num]
		num_packets = int(ceil(float(len(payload)) / packet_size))

		print "frame_num is %d, length of frame is %d" %(frame_num, len(payload))

		# receiver needs to know total number of packets for each frame
		# keep sending num_packets until ACKs from all receivers received
		receiver_acks = 0
		while True:
			try:
				send_again_flag = False
				sent_bytes = sender.sendto("META" + str(num_packets), multicast_group)
				while True:
					try:
						data, receiver = sender.recvfrom(1024)
						if "R1 START ACK" in data:
							print data
							receiver_acks += 1
						elif num_receivers > 1:
							if "R2 START ACK" in data:
								print data
								receiver_acks += 1
						elif num_receivers > 2:
							if "R3 START ACK" in data:
								print data
								receiver_acks += 1
						else:
							pass
						if receiver_acks == num_receivers:
							break
					except socket.timeout:
						receiver_acks = 0
						send_again_flag = True
						break
				if send_again_flag == False:
					break
			except:
				pass
				
		for i in range(0, num_packets):

			# transmit everything in retransmission queue before sending future packets
			while not q.empty():
				p_index = q.pop()
				encoded_msg = setup.get_packet_data(p_index, frame_num, packet_size, payload)
				if p_index != num_packets - 1:
					encoded_msg = coder.encode_fast(encoded_msg)
				try:
					sent_bytes = sender.sendto(encoded_msg, multicast_group)
				except:
					print "Error with sending message for retransmission packet %d" %(p_index)
					sys.exit(-1)

			encoded_msg = setup.get_packet_data(i, frame_num, packet_size, payload)
			if i != num_packets - 1:
				encoded_msg = coder.encode_fast(encoded_msg)

			try:
				sent_bytes = sender.sendto(encoded_msg, multicast_group)
				while True:
					try:
						data, receiver = sender.recvfrom(1024)
						if data:
							print data
							if "RP" in data:
								re_num = int(data[2:])
								if not q.contains(re_num):
									q.push(re_num)
					except socket.timeout:
						break
			except:
				print "Error with sending message for packet %d" %(i)
				sys.exit(-1)

		# sender sends EOF packet and sends retransmission packets until all receivers have all packets
		# for current frames
		comp_frame_acks = 0
		while True:
			try:
				send_again_flag = False
				sent_bytes = sender.sendto("EOF", multicast_group)
				while True:
					try:
						data, receiver = sender.recvfrom(1024)
						if "RP" in data:
							re_num = int(data[2:])
							if not q.contains(re_num):
								q.push(re_num)
						elif "R1 DONE ACK" in data:
							print data
							comp_frame_acks += 1
						elif num_receivers > 1:
							if "R2 DONE ACK" in data:
								print "R2 DONE ACK"
								comp_frame_acks += 1
						elif num_receivers > 2:
							if "R3 DONE ACK" in data:
								print data
								comp_frame_acks += 1
						if comp_frame_acks == num_receivers:
							break
					except socket.timeout:
						comp_frame_acks = 0
						send_again_flag = True
						break
				if send_again_flag == False:
					break
				elif not q.empty():
					while not q.empty():
						p_index = q.pop()
						encoded_msg = setup.get_packet_data(p_index, frame_num, packet_size, payload)
						if p_index != num_packets - 1:
							encoded_msg = coder.encode_fast(encoded_msg)
						try:
							sent_bytes = sender.sendto(encoded_msg, multicast_group)
						except:
							print "Error with sending message for retransmission packet %d" %(p_index)
							sys.exit(-1)
				else:
					pass

			except:
				pass

if __name__ == "__main__":
	main()