Error Correction in Video Multicasting

Project Topic: 
Evaluate the prospective of reliably sending video data in the 802.11 protocol to multiple receivers by using multicast and an implementation of error correction. Packet loss in multicast is prevalent since unicast streaming protocols such as RTS/CTS aren't applied. Furthermore, receivers can lose different packets, lose the same packet and request retransmission at the same time, or have the order of their packets wrong. All these scenarios will have to be resolved using some error correction design. 

