#!/bin/bash
modprobe ifb
ip link set dev ifb0 up
tc qdisc add dev wlo1 ingress
tc filter add dev wlo1 parent ffff: \protocol ip u32 match u32 0 0 flowid 1:1 action mirred egress redirect dev ifb0
tc qdisc add dev ifb0 root netem delay 750ms
