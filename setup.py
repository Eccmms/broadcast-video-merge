import os
import sys
import subprocess
import shlex
import video_frames
import sender

def find_parity(s):

	num = 0
	for char in s:
		if char == "1":
			num += 1
	return num % 2

def init():

	# if frames directory already exists, remove
	pwd = os.getcwd()
	if "frames" in os.listdir(pwd):
		try:
			args = shlex.split("rm -r frames")
			subprocess.check_call(args)
		except subprocess.CalledProcessError:
			print "Error with removing frames directory"
			sys.exit(1)

	# create a directory called "frames" and populate it with frames of .mp4 file
	video_name = "low_quality.mp4"
	num_frames = video_frames.main(video_name)

	# populate list of bytearrays where the bytearrays are for each jpg image
	frames_payloads = []
	for i in range(0, num_frames):
		jpg_name = "frame" + str(i) + ".jpg"
		with open("frames/" + jpg_name, "rb") as imgfile:
			data = imgfile.read()
		frames_payloads.append(bytearray(data))
	return frames_payloads

# packet num is the designated packet bytes needed from frame_data
def get_packet_data(packet_num, frame_num, packet_size, frame_data):

	start =  packet_num * packet_size 
	if start + packet_size < len(frame_data):
		end = start + packet_size
	else:
		end = len(frame_data)
	packet_bytes = frame_data[start:end]
	packet_binary = ""
	packet_binary_string = ""
	for byte in packet_bytes:
		packet_binary_string += chr(byte)
		packet_binary += format(byte, '08b')
	frame_byte = chr(frame_num)
	seq_binary = format(packet_num, '010b')
	middle_byte = chr(int("00000" + seq_binary[:3], 2))
	last_byte = chr(int(seq_binary[3:] + str(find_parity(packet_binary)), 2))
	packet_binary_string += (frame_byte + middle_byte + last_byte)
	return packet_binary_string