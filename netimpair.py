import sys 
import os
import subprocess
import shlex

def get_commands():

    commands = ""
    if raw_input("Do you want packet delay? (yes/no) ") == "yes":
        commands += (" delay " + raw_input("By how many ms? ") + "ms" )
    if raw_input("Do you want packet corruption? (yes/no) ") == "yes":
        commands += (" corrupt " + raw_input("What percentage of the packets? ") + "%")
    if raw_input("Do you want packet loss? (yes/no) ") == "yes":
        commands += (" loss " + raw_input("What percentage of the packets? ") + "%")
    return commands

def reset():

    try:
        args = shlex.split("tc qdisc del dev wlan0 root")
        subprocess.check_call(args)
    except:
        print "Ignore external RTNETLINK warning"
        
    try:
        args = shlex.split("tc qdisc del dev wlan0 ingress")
        subprocess.check_call(args)
    except:
        print "Ignore local RTNETLINK warning"

def init_ifb():

    try:
        args = shlex.split("modprobe ifb")
        subprocess.check_call(args)
        args = shlex.split("ip link set dev ifb0 up")
        subprocess.check_call(args)
        args = shlex.split("tc qdisc add dev wlan0 ingress")
        subprocess.check_call(args)
        args = shlex.split("tc filter add dev wlan0 parent ffff: protocol ip u32 match u32 0 0 flowid 1:1 action mirred egress redirect dev ifb0")
        subprocess.check_call(args)

    except subprocess.CalledProcessError:
        print "Error in initializing IFB"
        sys.exit(-1)

def main():

    if os.geteuid():
        print "Please run script as root by using \"sudo su\""
        sys.exit(1)

    if raw_input("Do you want to reset network to default settings and exit? (yes/no) ") == "yes":
        reset()
        sys.exit(0)

    reset()

    if raw_input("Where are the receivers? (local/external) ") == "local":
        init_ifb()
        interface = "ifb0"
        action = "change"
    else:
        interface = "wlan0"
        action = "add"

    commands = get_commands()
    try:
        args = shlex.split("tc qdisc " + action + " dev " + interface + " root netem" + commands)
        subprocess.check_call(args)
    except subprocess.CalledProcessError:
        print "Error running network commands"
        sys.exit(-1)

    print "Network impaired successfully"

if __name__ == "__main__":
    main()