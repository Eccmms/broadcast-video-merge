# Basic server setup that will send a client a response

import socket
import signal
import sys

def sig_int_handler(signal, frame):

	print "SERVER CLOSING"
	sys.exit(0)

def main():

	# handle server interrupts such as Ctrl-C
	signal.signal(signal.SIGINT, sig_int_handler)

	# create socket object that supports IP4 address family and uses UDP
	server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

	# bind server to host_name and port 
	host = socket.gethostname()
	port = 7777
	server.bind((host, port))

	print "SERVER STARTING"

	while True:

		# receive client connection message and address
		client_data, client_addr = server.recvfrom(4096)

		# send message to client
		if client_data:
			print client_data
			message = "Hello client!"
			sent_bytes = server.sendto(message, client_addr)
			
if __name__ == "__main__":
	main()
