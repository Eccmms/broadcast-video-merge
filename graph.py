import numpy as np 
import matplotlib.pyplot as plt 
import matplotlib.animation as animation
from random import randint

N = 3
x_locs = np.arange(N)
width = 0.35
y_vals = [20, 30, 100]

fig, ax = plt.subplots()
rects = ax.bar(x_locs, y_vals, width, color="b", align="center", label="R1")

ax.set_title("Multicast Transmission")
ax.set_ylabel("Percentage")
ax.set_xticks(x_locs)
ax.set_xticklabels(("Packets Successful", "Packets Corrupted", "Packets Lost"))
ax.legend()

def animate(i):

	for rect in rects:
		rect.set_height(randint(0, 100))
	return rects

ani = animation.FuncAnimation(fig, animate, interval=500, blit=True)
plt.show()
