# Basic class implementation for a queue

class Queue:

	def __init__(self):
		self.items = []

	def empty(self):
		if len(self.items) == 0:
			return True
		return False

	def push(self, p_index):
		self.items.append(p_index)

	def pop(self):
		return self.items.pop(0)

	def contains(self, p_index):
		if p_index in self.items:
			return True
		return False

	def size(self):
		return len(self.items)



