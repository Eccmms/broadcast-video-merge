# Basic client setup that will receive data from a server

import socket

def main():

	# create client socket that is of IPv4 address family and uses UDP
	client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

	# connect client to server port and hostname
	host = socket.gethostname()
	port = 7777

	# send initial message to server and receive response
	message = "Client is connected"
	sent_bytes = client.sendto(message, (host, port))

	server_response, server_addr = client.recvfrom(4096)
	print server_response

	# close client socket
	client.close()
	
if __name__ == "__main__":
	main()